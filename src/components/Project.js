import {useState} from "react";

export default ({project})=>{

    const [name,setName] = useState(project.get("name"))
    const enterName = (name)=> {
        setName(name)
        project.set("name",name)
    }



    return (
        <div>
            <h1>{name}</h1>
            <input type={'text'} placeholder={name} value={name} onChange={ (e)=> enterName(e.target.value)} />

        </div>
    )

}