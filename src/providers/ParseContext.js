import {createContext , useContext } from 'react'
import Parse from 'parse'

const ParseContext = createContext({})

export function ParseContextProvider({ props  }){

    Parse.serverURL =  "http://localhost:1337/1"
    Parse.initialize("myAppId")

    return (
        <ParseContext.Provider value={Parse}>
            {props.children}
        </ParseContext.Provider>
    )

}

export const useParseContext = ()=> useContext(ParseContext)


