import React from 'react';
import PropTypes from 'prop-types';
import {List, ListItem, ListItemText, makeStyles, Paper, Divider , Button} from "@material-ui/core";
import { useEffect , useState } from "react";
import { clone } from 'lodash'
import { useQuery } from 'react-query'
import exportSupplierStatus from './services/SupplierStatus.js'
import exportSupplier from './services/Supplier.js'
import { Object } from 'parse'
import initParse from "./helpers/initParse"
import { ParseContextProvider , useParseContext } from "./providers/ParseContext";

const fallbackData = ['name1', 'name2', 'name3', 'name4', 'name5',"name6"];

const useStyles = makeStyles((theme) => ({
  container: {
    paddingTop: "1.5rem",
    backgroundColor : "green"
  },
}));



export default function App({data}) {

  const input = data ? data : fallbackData;
  const {container} = useStyles();

  const Parse = initParse()

  const SupplierStatus = exportSupplierStatus(Parse)
  const [projects,setProjects] = useState([])

  const getData = async _=> {
      let pro  = []
      try {
          pro = await SupplierStatus.getAll()
         console.log(pro)
          setProjects( pro)
      }
        catch (e) {
          console.log(e)
        }
    }

    const saveAll = async _=> {
      await Object.saveAll(projects)
      setProjects(clone(projects))
  }

    useEffect( _=> {getData()},[] )

    return (
     <ParseContextProvider>
    <div className={container}>
        <h1>Test</h1>
      <Paper elevation={3}>
        <Button onClick={saveAll}> {'Alle speichern'} </Button>

        <List>
          {projects.map((p, index) => (
              <div key={index} >
                <div>{ p.supplier.name }</div>
              </div>
          ) ) }
        </List>
      </Paper>
    </div>
     </ParseContextProvider>
  )

}

App.propTypes = {
    data: PropTypes.string.isRequired
}
