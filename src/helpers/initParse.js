import Parse from 'parse'

export default ()=>{
    if (!Parse.applicationId){
        Parse.serverURL =  "http://localhost:1337/1"
        Parse.initialize("myAppId")
    }
    return Parse
}