export default  (Parse)=> {
    const Supplier = class Supplier extends Parse.Object {
        constructor() {
            super("Supplier");
        }

        get name() {
            return this.get("name")
        }

    }
    Parse.Object.registerSubclass("Supplier",Supplier)
    return Supplier
}

