import exportSupplier from './Supplier'

const exportSupplierStatus = (Parse)=> {

    const Supplier = exportSupplier(Parse)

    const SupplierStatus = class SupplierStatus extends Parse.Object {
        constructor() {
            super("SupplierStatus");
        }

        get supplier(){
            return this.get("supplier")
        }

        set supplier(supplier){
            return this.set("supplier",supplier)
        }

        get state(){
            return this.get("state")
        }

        static getAll(){
            return new Parse.Query(this).include("supplier").find()
        }

    }
    Parse.Object.registerSubclass("SupplierStatus",SupplierStatus)
    return SupplierStatus
}

export default exportSupplierStatus
