import initParse from './helpers/initParse.js'
import SupplierStatus from './services/SupplierStatus.js'

const Parse = initParse()

const register = ()=>{
    [SupplierStatus].forEach( cl => Parse.Object.registerSubclass(cl.getClassName(),cl) )
}

register()


export {
    SupplierStatus
}

