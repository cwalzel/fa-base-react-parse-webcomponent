import React from "react";
import ReactDOM from "react-dom";
import reactToWebComponent from "react-to-webcomponent";
import App from "./App";


const app = reactToWebComponent(App, React, ReactDOM);
const COMPONENT_NAME = "app-webcomponent"

customElements.define(COMPONENT_NAME, app);
